# README #

### What is it? ###

This is a tool for measuring the latency for random RAM access.
To avoid prefetching the data is accessed in a random and cycle free walk without repeated access to the same field.
Furthermore a cacheline will never be accessed twice before all other fields of the array were accessed.

### Installation ###

```bash
cmake ./
make
```

### How to use? ###

Arguments:
* -s [size of the array in Bytes]
* -w [number of random accesses to measure]
* --seed [seed for random walk]

Example:
```bash
./MemLookup_Bench -w 10000000 -s 5000000000
```

### License

The framework is published under the [MIT License](LICENSE).

### Dependencies
This project uses the args project for handling command line arguments:
https://github.com/Taywee/args
