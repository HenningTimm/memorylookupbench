include(FindPackageHandleStandardArgs)

set(Args_DOWNLOAD_DIR "${CMAKE_CURRENT_BINARY_DIR}/tmp/Args")
set(Args_INSTALL_DIR "${CMAKE_CURRENT_BINARY_DIR}/external/Args")

set(Args_INCLUDE_DIRS ${Args_INSTALL_DIR})
# Set library names
# set(Args_LIBRARIES
#         ${Args_INSTALL_DIR}/lib/libdivsufsort.a
#         ${Args_INSTALL_DIR}/lib/libdivsufsort64.a
#         ${Args_INSTALL_DIR}/lib/libsdsl.a)

message(STATUS "Removing remains of previously downloaded Args versions")
file(REMOVE_RECURSE ${Args_DOWNLOAD_DIR})
file(REMOVE_RECURSE ${Args_INSTALL_DIR})

include(ExternalProject)
ExternalProject_Add(
    Args-download
    PREFIX ${Args_DOWNLOAD_DIR}
    GIT_REPOSITORY https://github.com/Taywee/args.git
    # Use fixed version (same as tudocomp)
#    GIT_TAG 7bbb71e8e13279ab45111bd135f4210545cd1c85
    PATCH_COMMAND ""
    UPDATE_COMMAND ""
    CONFIGURE_COMMAND ""
    # Copy Make.helper to install dir. Necessary for FindGCSA2 to work
    BUILD_COMMAND mkdir ${Args_INSTALL_DIR} && cp "args.hxx" "${Args_INSTALL_DIR}/"
    BUILD_IN_SOURCE TRUE
    INSTALL_COMMAND ""
)
add_dependencies(external-downloads Args-download)

FIND_PACKAGE_HANDLE_STANDARD_ARGS(Args DEFAULT_MSG Args_INCLUDE_DIRS)

set(DIVSUFSORT_FOUND ${Args_FOUND})
set(DIVSUFSORT64_FOUND ${Args_FOUND})
